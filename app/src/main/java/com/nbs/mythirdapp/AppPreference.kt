package com.nbs.mythirdapp

import android.content.Context
import android.content.SharedPreferences

class AppPreference(context: Context) {

    var userPreference: SharedPreferences ?= null

    init {
        userPreference = context
            .getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE)
    }

    fun setValue(key: String, value: String){
        userPreference?.edit()?.putString(key, value)?.apply()
    }

    fun getStringValue(key: String): String? =
            userPreference?.getString(key, null)

    fun setValue(key: String, value: Int){
        userPreference?.edit()?.putInt(key, value)?.apply()
    }

    fun getIntValue(key: String): Int? =
            userPreference?.getInt(key, 0)

    fun clear(){
        userPreference?.edit()?.clear()?.apply()
    }

}