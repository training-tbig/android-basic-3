package com.nbs.mythirdapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var appPreference: AppPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        appPreference = AppPreference(this)

        if (!TextUtils.isEmpty(appPreference.getStringValue(PREF_KEY_NAME))){
            edtName.setText(appPreference.getStringValue(PREF_KEY_NAME))
        }

        if (!TextUtils.isEmpty(appPreference.getStringValue(PREF_KEY_PHONE))){
            edtPhoneNumber.setText(appPreference.getStringValue(PREF_KEY_PHONE))
        }

        if (!TextUtils.isEmpty(appPreference.getStringValue(PREF_KEY_EMAIL))){
            edtEmail.setText(appPreference.getStringValue(PREF_KEY_EMAIL))
        }

        edtAge.setText(appPreference.getIntValue(PREF_KEY_AGE).toString())

        btnSave.setOnClickListener {
            val name = edtName.text.toString().trim()
            val email = edtEmail.text.toString().trim()
            val phone = edtPhoneNumber.text.toString().trim()
            val age = edtAge.text.toString().trim()

            appPreference.setValue(PREF_KEY_NAME, name)
            appPreference.setValue(PREF_KEY_EMAIL, email)
            appPreference.setValue(PREF_KEY_PHONE, phone)
            appPreference.setValue(PREF_KEY_AGE, age.toInt())

            Toast.makeText(this, "Data saved",
                Toast.LENGTH_LONG).show()
        }

        btnClear.setOnClickListener {
            appPreference.clear()

            edtName.setText("")
            edtEmail.setText("")
            edtPhoneNumber.setText("")
            edtAge.setText("0")
        }
    }
}
