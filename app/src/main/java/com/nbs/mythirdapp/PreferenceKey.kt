package com.nbs.mythirdapp

const val PREF_KEY_NAME = "key_name"
const val PREF_KEY_EMAIL = "key_email"
const val PREF_KEY_PHONE = "key_phone"
const val PREF_KEY_AGE = "key_age"

const val PREFERENCE_NAME = "PREF_USERDATA"